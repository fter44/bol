--[[
FREE ELISE 

BY DIENOFAIL

CHANGELOG

v0.01 - RELEASE



]]


if myHero.charName ~= "Elise" then return end

require "FastCollision"
require "Prodiction"

if not VIP_USER then
	print("Elise is a VIP only script, cause I'm too lazy to make it for free users")
	return
end

local Prodict = ProdictManager.GetInstance()
local ProdictE = Prodict:AddProdictionObject(_E, 1100, 1300, 0.1515, 70)
local ProdictSpiderE = Prodict:AddProdictionObject(_E, 1100, math.huge, 1, 0)
local ProdictSpiderE2 = Prodict:AddProdictionObject(_E, 1100, math.huge, 0.250, 0)
local Collision = FastCol(ProdictQ)
local isSpider = false
local isRappel = false
local RappelTimer = 0
local RappelStartTimer = 0
local RappelMaxTimer = 2000 
local SpiderQcd, SpiderWcd, SpiderEcd = nil, nil, nil
local HumanQcd, HumanWcd, HumanEcd = nil, nil, nil
local isSafe = true
local HumanSkillQ = {spellkey = _Q, range = 625, speed = math.huge, delay = 250, width = 0}
local HumanSkillE = {spellkey = _E, range = 1100, speed = 130, delay = 151.5, width = 70}
local HumanSkillW = {spellkey = _W, range = 950, speed = math.huge, delay = 300, width = 0}
local SpiderSkillQ = {spellkey = _Q, range = 475, speed = math.huge, delay = 250, width = 0}
local SpiderSkillW = {spellkey = _W, range = math.huge, speed = math.huge, delay = 250, width =0}
local SpiderSkillE = {spellkey = _E, range = 1100, speed = math.huge, delay = 250, width = 0}
AdvancedCallback:bind('OnGainBuff', function(unit, buff) OnGainBuff(unit, buff) end)
AdvancedCallback:bind('OnLoseBuff', function(unit, buff) OnLoseBuff(unit, buff) end)
local KeyZ = string.byte("Z")
local KeyX = string.byte("X")
local KeyC = string.byte("C")
function PluginOnLoad()
	AutoCarry.PluginMenu:addParam("JungleFarm", "Farm Jungle", SCRIPT_PARAM_ONKEYDOWN, false, KeyZ)
	AutoCarry.PluginMenu:addParam("EmergencySnare", "Emergency Snare", SCRIPT_PARAM_ONKEYDOWN, false, KeyX)
	AutoCarry.PluginMenu:addParam("EmergencyEscape", "Emergency Escape", SCRIPT_PARAM_ONKEYDOWN, false, KeyC)
end


function Menu()



end

function PluginOnTick()
	CheckRappelTimer()



end

local function getHitBoxRadius(target)
	return GetDistance(target, target.minBBox)
	--return GetDistance(target.minBBox, target.maxBBox)/4
end

function CastHumanQ(Target)

end

function CastHumanW(Target)


end

function CastHumanE(Target)
	local ECastPosition, ETime, EHitChance = ProdictE:GetPrediction(Target)
	if Target ~= nil and GetDistance(ECastPosition, myHero) < HumanSkillE.range then
		local WillCollide = Collision:GetMinionCollision(ECastPosition, myHero)
		if not WillCollide and EHitChance > 0.5 then
			CastSpell(_E, ECastPosition.x, ECastPosition.z)
		end
	end
end


function CastSpiderQ(Target)


end

function CastSpiderW(Target)


end


function CastSpiderE(Target, mode, delay)
	--If mode == 1, cast to target immediately
	--If mode == 2, hide then cast to escape/cast to enemy champion
	--If mode == 3, hide then cast to escape/cast to minion
	--If mode == 4, dodge skillshot
	local ECastPosition, ETime, EHitChance = ProdictSpiderE:GetPrediction(Target)
	local E2CastPosition, E2Time, E2HitChance = ProdictSpiderE2:GetPrediction(Target)
	if mode == 1 and ValidTarget(Target) and GetDistance(ECastPosition, myHero) < SpiderSkillE.range and (delay == 0 or delay == nil) then
		if not isRappel then
			CastSpell(_E, Target)
		end
	elseif mode == 2 and ValidTarget(Target) and GetDistance(E2CastPosition, myHero) < SpiderSkillE.range then
		if not isRappel then
			CastSpell(_E)
		elseif RappelTimer >= delay and isRappel
			CastSpell(_E, Target)
		end
	elseif mode == 3 and ValidTarget(Target) and GetDistance(Target, myHero) < SpiderSkillE.range then
		if not isRappel then
			CastSpell(_E)
		elseif not isRappel and (delay == 0 or delay == nil)
			CastSpell(_E, Target)
		elseif RappelTimer >= delay and isRappel
			CastSpell(_E, Target)
		end
	elseif mode == 4 then
		if not isRappel then
			CastSpell(_E)
		elseif isRappel and isSafe and GetDistance(Target, myHero) < SpiderSkillE.range and RappelTimer >= delay then
			CastSpell(_E, Target)
		end 
	end
end


function CheckForm()
	local SpellQ = myHero:GetSpellData(_Q)
	if SpellQ.name == 'EliseHumanQ'
		isSpider = false
	else
		isSpider = true
	end
end

function OnGainBuff(unit, buff)
	if buff.name = 'elisespidere' and unit.isMe then
		isRappel = true
		RappelStartTimer = GetTickCount()
	end
end

function OnLoseBuff(unit, buff)
	if buff.name = 'elisespidere' and unit.isMe then
		isRappel = 0
		RappelTimer = 0
		RappelStartTimer = nil
	end
end

function CheckRappelTimer()
	if isRappel and RappelStartTimer ~= nil then
		RappeTimer = GetTickCount() - RappelStartTimer
	end
end

function Checks()
	Target = AutoCarry.GetAttackTarget()


end

